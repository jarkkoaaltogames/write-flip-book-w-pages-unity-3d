# Write Flip Book W Pages Unity 3D

In this mini course you will create a Book with pages that you can use in your own games and apps.

I will show you how to find and create assets for the game, (plus: you will also be able to download resource zip files).

I will also show you have to build assets in the unity and write scripts that make the objects work, and it is all put together in a FlipBook App.

Fantasy Fonts:
https://www.1001fonts.com/fantasy-fonts.html

Free Sounds:
https://www.soundjay.com/page-flip-sounds-1.html

Book cover pictures:
https://www.organise-us.com/

### Book Cover
'
![FlibBook](/img/book_cover.png)

### Book Open and Text both page

![FlibBook](/img/book_open.png)

### Book with Paground Picture

![FlibBook](/img/withBackground.png)