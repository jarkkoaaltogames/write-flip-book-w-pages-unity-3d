﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour {

	public Button okButton;
	public Text hiText;
	// Use this for initialization
	void Start () {
		if (okButton != null && hiText != null) 
			okButton.onClick.AddListener (() => okButton_Click("Hi There"));
		
	}


	private void okButton_Click(string msg){
		hiText.text = msg;	
	}

}