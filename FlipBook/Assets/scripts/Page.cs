﻿using System.Collections;
using System.Collections.Generic;

public class Page
{
    public string Title { get; set; }
    public string Text { get; set; }
    public List<string> Pages { get; set; }

    private static List<Page> _pageList = null;

    public static Page RandomPage;

    public static int CurrentPage1 = 0;
    public static int CurrentPage2 = 1;

    public static Page GetRandomPage()
    {
        List<Page> pageList = Page.PageList;

        int num = UnityEngine.Random.Range(0, pageList.Count);
        Page pge = pageList[num];
        pge.Pages = new List<string>();

        string[] words = pge.Text.Split(' ');
        //put 7 words on each page
        string page = "";
        int wordCnt = 0;

        foreach (string word in words)
        {
            wordCnt++;
            if (wordCnt > 10)
            {
                pge.Pages.Add(page);
                page = "";
                wordCnt = 0;
            }
            page += string.Format("{0} ", word);
        }
        pge.Pages.Add(page);

        RandomPage = pge;

        return pge;
    }



    public static List<Page> PageList
    {
        get
        {
            if (_pageList == null)
            {
                _pageList = new List<Page>();

                _pageList.Add(new Page
                {  //1
						Title = "Lorem ipsum dolor",
						Text = " Aenean egestas, nisl vel posuere tristique, est urna varius odio, pulvinar sodales mauris dolor nec justo. Vivamus tempor ac tellus ut sagittis. Curabitur sagittis hendrerit sem, et aliquet lorem. "
                });

                _pageList.Add(new Page
                {  //2
						Title = "Nunc mattis",
						Text = "Pellentesque a tellus ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras vehicula ex facilisis, placerat risus eu, iaculis massa."
                });

                _pageList.Add(new Page
                {  //3
						Title = "Mauris in pretium turpis",
						Text = "Nunc cursus est nulla, condimentum hendrerit arcu consectetur eget. Maecenas ac ultrices turpis."
                });

                _pageList.Add(new Page
                {  //4
						Title = "Donec eleifend",
						Text = "Mauris egestas commodo ipsum nec blandit. Cras malesuada, felis at finibus vulputate, ligula felis malesuada sapien, egestas volutpat est ex nec purus."
                });

                _pageList.Add(new Page
                {  //5
						Title = "Aliquam at velit",
						Text = "Nunc convallis molestie turpis, quis rutrum mi. Praesent porttitor tellus urna, quis lobortis orci elementum non."
                });

                _pageList.Add(new Page
                {  //6
						Title = "Fusce nec facilisis",
						Text = "Aliquam eu ultricies justo. Sed ac dignissim turpis. Cras ante tellus, pulvinar vel facilisis ut, porta sit amet sem."
                });

                _pageList.Add(new Page
                {  //7
						Title = "Etiam tempor",
						Text = "Nulla facilisi. Curabitur ut nulla eu nulla bibendum tempus. Mauris fringilla eu risus vitae luctus."
                });

                _pageList.Add(new Page
                {  //8
						Title = "Fusce sagittis ",
						Text = "Nunc lacus magna, sagittis hendrerit mattis vitae, sollicitudin at erat. Nulla sed dolor purus."
                });

                _pageList.Add(new Page
                {  //9
						Title = "Cras ac odio",
						Text = "Suspendisse potenti. Proin nibh nibh, vestibulum quis tincidunt eu, varius at nunc. Nam et purus lorem."
                });

            }

            return _pageList;
        }
    }
}